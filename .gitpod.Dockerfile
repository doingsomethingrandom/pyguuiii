FROM gitpod/workspace-full-vnc

RUN sudo apt-get update && \
    sudo apt-get upgrade -y && \
    sudo rm -rf /var/lib/apt/lists/*
